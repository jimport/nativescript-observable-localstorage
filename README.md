# nativescript-observable-localstorage

nativescript-observable-localstorage currently only supports the storage of 
JSON objects. These objects are stored in files in the apps data folder.

## Usage

First import the plugin:

```javascript
import { ObservableLocalStorage } from 'nativescript-observable-localstorage';
```

In order to access the stored information, you need to subscribe to the item 
which is stored under the key 'settings'

```javascript
// get an instance of the store
let store = ObservableLocalStorage.getInstance();

// and subscribe to it
store.getItem('settings')subscribe(data => {
  console.log(JSON.stringify(data));
});

```

This stores an item under the key 'settings' and triggers the change event

```javascript 
// get an instance of the store
let store = ObservableLocalStorage.getInstance();

// and store a JSON object
store.setItem('settings', JSON.parse(jsonString));

// ... or
store.setItem('settings', JSON.stringify(jsonObject));
```