import { BehaviorSubject } from 'rxjs/BehaviorSubject';
export declare class ObservableLocalStorage {
    private static INSTANCE;
    private _subjects;
    private _keys;
    private _storageLocation;
    private constructor();
    static getInstance(): ObservableLocalStorage;
    getItem(key: any): BehaviorSubject<any>;
    setItem(key: string, value: any): Promise<any>;
    private __writeJson(key, value);
    private __readJson(filename);
    private __registerSubject(key);
    private __getDataFolder();
    private __initializeStorageLocation();
}
