
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import * as _fs from 'tns-core-modules/file-system';

export class ObservableLocalStorage {

  private static INSTANCE: ObservableLocalStorage;

  private _subjects: BehaviorSubject<any>[] = [];
  private _keys: string[] = [];

  private _storageLocation = '/data/';

  private constructor() {
    console.log("creating instance of ObservableLocalStorage");
    this.__initializeStorageLocation();
  }

  public static getInstance() {
    if (!this.INSTANCE) {
      this.INSTANCE = new ObservableLocalStorage();
    }
    return this.INSTANCE;
  }

  public getItem(key) {
    let i = this.__registerSubject(key);
    return this._subjects[i];
  }
  
  public setItem(key: string, value: any) {
    let i = this.__registerSubject(key);
    this._subjects[i].next(JSON.parse(value));
    return this.__writeJson(key, value);
  }

  private __writeJson(key: string, value: any) {
    let data = (typeof value === 'string') ? value : JSON.stringify(value);
    let fileName = key + '.json';
    let file = this.__getDataFolder().getFile(fileName);
    return file.writeText(data);
  }

  private __readJson(filename) {
    return this.__getDataFolder().getFile(filename).readText();
  }

  private __registerSubject(key: string) {
    let keyIndex = this._keys.indexOf(key);
    if (keyIndex === -1) {
      this._keys.push(key);
      this._subjects.push(new BehaviorSubject<any>({}));
      keyIndex = this._subjects.length - 1;
    }
    return keyIndex;
  }

  private __getDataFolder() {
    return _fs.Folder.fromPath(_fs.path.join(
      _fs.knownFolders.currentApp().path, this._storageLocation));
  }

  private __initializeStorageLocation() {
    this.__getDataFolder().getEntities().then(res => {
      res.forEach(entity => {
        let key = entity.name.split('.')[0];
        let keyIndex = this.__registerSubject(key);
        this.__readJson(entity.name).then(res => {
          this._subjects[keyIndex].next(JSON.parse(res));
        })
      })
    })
  }


}
